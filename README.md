﻿# vpps-res-ami-amqp

Asterisk IPBX module for publish AMI events to an AMQP server.  

To install

```shell
yum install gcc make cmake3 libtool asterisk-devel vpps-res-amqp-devel git
git clone https://gitlab.com/vppsvoip/vpps-res-ami-amqp.git
cd vpps-res-ami-amqp
mkdir build
pushd build
cmake -DCMAKE_INSTALL_PREFIX=/ -DCMAKE_BUILD_TYPE=Release ..
make
sudo make install
popd
```

Configure the file in `/etc/asterisk/res_ami_amqp.conf`.  

To load module:

```
CLI> module load res_ami_amqp.so
```

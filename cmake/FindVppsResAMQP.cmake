# Locate Asterisk PBX res_amqp.so module
# This module defines
# RES_AMQP_FOUND YES, if res_amqp.h founded
# RES_AMQP_H, full path to res_amqp.h file

set(RES_AMQP_FOUND "NO")

find_file(RES_AMQP_H res_amqp.h
    PATH_SUFFIXES asterisk
    PATHS /usr/include
)

if(RES_AMQP_H)
    set(RES_AMQP_FOUND "YES")
endif()

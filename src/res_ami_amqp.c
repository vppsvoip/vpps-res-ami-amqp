/*
 * Asterisk -- An open source telephony toolkit.
 *
 * Copyright 2020 VOIP+, <support@voip.plus>
 * Copyright 2020 Maxim Stjazhkin, <maxim.stjazhkin@gmail.com>
 * Copyright 2015-2017 The Wazo Authors  (see the AUTHORS file)
 *
 * David M. Lee, II <dlee@digium.com>
 *
 * See http://www.asterisk.org for more information about
 * the Asterisk project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

/*! \file
 *
 * \brief AMQP client APIs for AMQP.
 *
 * This is mostly a thin wrapper around some of the <a href="http://alanxz.github.io/rabbitmq-c/docs/0.5.0/">rabbitmq-c
 * APIs</a>, with additional features for thread safety and connection management.
 */

/*** MODULEINFO
    <depend>res_amqp</depend>
    <defaultenabled>no</defaultenabled>
    <support_level>extended</support_level>
 ***/

/*** DOCUMENTATION
    <configInfo name="res_ami_amqp" language="en_US">
        <synopsis>AMI to AMQP Backend</synopsis>
        <configFile name="res_ami_amqp.conf">
            <configObject name="global">
                <synopsis>Global configuration settings</synopsis>
                <configOption name="enabled">
                   <synopsis>Enable/disable the AMQP AMI module</synopsis>
                   <description>
                       <para>This option enables or disables the AMQP module.</para>
                   </description>
               </configOption>
                <configOption name="connection">
                    <synopsis>Name of the connection from amqp.conf to use</synopsis>
                    <description>
                        <para>Specifies the name of the connection from amqp.conf to use</para>
                    </description>
                </configOption>
                <configOption name="exchange">
                    <synopsis>Name of the exchange to post to</synopsis>
                    <description>
                        <para>Defaults to empty string</para>
                    </description>
                </configOption>
            </configObject>
        </configFile>
    </configInfo>
 ***/

#ifndef AST_MODULE
#define AST_MODULE "res_ami_amqp"
#endif

#include <asterisk.h>

ASTERISK_FILE_VERSION(__FILE__, "$Revision$")

#include <asterisk/config.h>
#include <asterisk/config_options.h>
#include <asterisk/json.h>
#include <asterisk/manager.h>
#include <asterisk/module.h>
#include <asterisk/res_amqp.h>

#define CONFIG_FILENAME "res_ami_amqp.conf"

/*! \brief The global options available for this module
 * \note This replaces the individual static variables that were previously present
 */
struct global_options
{
    char connection[255];
    char exchange[255];
    /*! Enabled by default, disabled if false. */
    int enabled;
    struct ast_amqp_connection *amqp;
};

/*! \brief All configuration objects for this module
 * \note If we had more than just a single set of global options, we would have
 *       other items in this struct
 */
struct module_config
{
    struct global_options *global; /*< Our global settings */
};

/*! \brief A container that holds our global module configuration */
static AO2_GLOBAL_OBJ_STATIC(module_configs);

/*! \brief A mapping of the module_config struct's general settings to the context
 *         in the configuration file that will populate its values */
static struct aco_type global_option = {
    .type = ACO_GLOBAL,
    .name = "global",
    .category = "^global$",
    .item_offset = offsetof(struct module_config, global),
    .category_match = ACO_WHITELIST,
};

/*! \brief A configuration file that will be processed for the module */
static struct aco_file module_conf = {
    .filename = CONFIG_FILENAME,        /*!< The name of the config file */
    .types = ACO_TYPES(&global_option), /*!< The mapping object types to be processed */
};

static void *module_config_alloc(void);
static void module_config_destructor(void *obj);
static int setup_amqp(void);

CONFIG_INFO_STANDARD(cfg_info, module_configs, module_config_alloc, .files = ACO_FILES(&module_conf),
                     .pre_apply_config = setup_amqp);

static struct aco_type *global_options[] = ACO_TYPES(&global_option);

/*! \internal \brief Create a module_config object */
static void *module_config_alloc(void)
{
    struct module_config *cfg;

    if (!(cfg = ao2_alloc(sizeof(*cfg), module_config_destructor)))
    {
        return NULL;
    }

    if (!(cfg->global = ao2_alloc(sizeof(*cfg->global), NULL)))
    {
        ao2_ref(cfg, -1);
        return NULL;
    }

    return cfg;
}

/*! \internal \brief Dispose of a module_config object */
static void module_config_destructor(void *obj)
{
    struct module_config *cfg = obj;
    ao2_cleanup(cfg->global->amqp);
    ao2_cleanup(cfg->global);
}

static int setup_amqp(void)
{
    struct module_config *conf = aco_pending_config(&cfg_info);

    if (!conf)
    {
        return 0;
    }

    if (!conf->global)
    {
        ast_log(LOG_ERROR, "Invalid %s\n", CONFIG_FILENAME);
        return -1;
    }

    if (!conf->global->enabled)
    {
        ast_log(LOG_NOTICE, "AMQP AMI disabled\n");
        return -1;
    }

    /* Refresh the AMQP connection */
    ao2_cleanup(conf->global->amqp);
    conf->global->amqp = ast_amqp_get_connection(conf->global->connection);

    if (!conf->global->amqp)
    {
        ast_log(LOG_ERROR, "Could not get AMQP connection %s\n", conf->global->connection);
        return -1;
    }
    return 0;
}

static char *new_routing_key(const char *prefix, const char *suffix)
{
    RAII_VAR(char *, lowered_suffix, NULL, ast_free);

    char *routing_key = NULL;
    size_t routing_key_len = strlen(prefix) + strlen(suffix) + 1; /* "prefix.suffix" */

    if (!(lowered_suffix = ast_strdup(suffix)))
    {
        ast_log(LOG_ERROR, "failed to copy a routing key suffix\n");
        return NULL;
    }

    for (char *ptr = lowered_suffix; *ptr != '\0'; ptr++)
    {
        *ptr = (char)tolower(*ptr);
    }

    if (!(routing_key = ast_malloc(routing_key_len + 1)))
    {
        ast_log(LOG_ERROR, "failed to allocate a string for the routing key\n");
        return NULL;
    }

    if (!(snprintf(routing_key, routing_key_len + 1, "%s.%s", prefix, lowered_suffix)))
    {
        ast_free(routing_key);
        ast_log(LOG_ERROR, "failed to format the routing key\n");
        return NULL;
    }

    return routing_key;
}

static int ami_event_to_json(struct ast_json *json_out, const char *event_name, char *content)
{
    RAII_VAR(struct ast_json *, json_value, NULL, ast_json_unref);

    char *line = NULL;
    char *word = NULL;
    char *key = NULL;
    char *value = NULL;

    int res = 0;
    int is_cel = 0;

    if (strcmp("CEL", event_name) == 0)
    {
        is_cel = 1;
    }

    json_value = ast_json_string_create(event_name);
    if (!json_value)
    {
        ast_log(LOG_ERROR, "failed to create json string %s\n", event_name);
        return -1;
    }

    res = ast_json_object_set(json_out, "Event", ast_json_ref(json_value));
    if (res != 0)
    {
        return -1;
    }

    while ((line = strsep(&content, "\r\n")) != NULL)
    {
        key = NULL;
        value = NULL;

        while ((word = strsep(&line, ": ")) != NULL)
        {
            if (!key)
            {
                key = word;
            }
            else
            {
                value = line;
                break;
            }
        }

        if (is_cel != 0 && (strcmp("Extra", key) == 0))
        {
            json_value = ast_json_load_string(value, NULL);
        }
        else
        {
            json_value = ast_json_string_create(value);
        }

        if (!json_value)
        {
            continue;
        }

        res = ast_json_object_set(json_out, key, ast_json_ref(json_value));
        if (res != 0)
        {
            ast_log(LOG_ERROR, "failed to set json value %s: %s\n", key, value);
            return -1;
        }
    }

    return 0;
}

static int publish_to_amqp(const char *topic, struct ast_json *body)
{
    RAII_VAR(struct module_config *, conf, NULL, ao2_cleanup);
    RAII_VAR(char *, msg, NULL, ast_json_free);

    int res = 0;

    if ((msg = ast_json_dump_string(body)) == NULL)
    {
        ast_log(LOG_ERROR, "failed to convert json to string\n");
        return -1;
    }

    amqp_basic_properties_t props = {
        ._flags = AMQP_BASIC_DELIVERY_MODE_FLAG | AMQP_BASIC_CONTENT_TYPE_FLAG,
        .delivery_mode = 2, /* persistent delivery mode */
        .content_type = amqp_cstring_bytes("application/json"),
    };

    conf = ao2_global_obj_ref(module_configs);

    ast_assert(conf && conf->global && conf->global->amqp);

    res = ast_amqp_basic_publish(conf->global->amqp, amqp_cstring_bytes(conf->global->exchange),
                                 amqp_cstring_bytes(topic), 0, 0, &props, amqp_cstring_bytes(msg));

    return res;
}

/* The helper function is required by struct manager_custom_hook.
 * See __ast_manager_event_multichan for details */
static int ami_custom_hook(int category, const char *event, char *content)
{
    RAII_VAR(struct ast_json *, json, NULL, ast_json_unref);
    RAII_VAR(char *, routing_key, NULL, ast_free);
    RAII_VAR(char *, _content, NULL, ast_free);

    const char routing_key_prefix[] = "asterisk.ami";
    int res = 0;

    (void)category;

    _content = ast_strdup(content);

    if ((json = ast_json_object_create()) == NULL)
    {
        ast_log(LOG_ERROR, "failed to create json object\n");
        return -1;
    }

    res = ami_event_to_json(json, event, _content);
    if (res != 0)
    {
        ast_log(LOG_ERROR, "failed to create AMI message json payload for %s\n", content);
        return -1;
    }

    if ((routing_key = new_routing_key(routing_key_prefix, event)) == NULL)
    {
        return -1;
    }

    res = publish_to_amqp(routing_key, json);
    if (res != 0)
    {
        ast_log(LOG_ERROR, "Error publishing AMI to AMQP\n");
        return -1;
    }

    return 0;
}

static struct manager_custom_hook ami_hook = {
    .file = __FILE__,
    .helper = &ami_custom_hook,
};

/*! \internal \brief load handler
 * \retval AST_MODULE_LOAD_SUCCESS on success
 * \retval AST_MODULE_LOAD_DECLINE on failure
 */
static enum ast_module_load_result load_module(void)
{
    if (!ast_module_check("res_amqp.so"))
    {
        if (ast_load_resource("res_amqp.so") != AST_MODULE_LOAD_SUCCESS)
        {
            ast_log(LOG_ERROR, "Cannot load res_amqp, so res_ami_amqp cannot be loaded\n");
            return AST_MODULE_LOAD_DECLINE;
        }
    }

    if (aco_info_init(&cfg_info))
    {
        goto load_error;
    }

    aco_option_register(&cfg_info, "enabled", ACO_EXACT, global_options, "yes", OPT_BOOL_T, 1,
                        FLDSET(struct global_options, enabled));

    aco_option_register(&cfg_info, "connection", ACO_EXACT, global_options, NULL, OPT_CHAR_ARRAY_T, 0,
                        CHARFLDSET(struct global_options, connection));

    aco_option_register(&cfg_info, "exchange", ACO_EXACT, global_options, NULL, OPT_CHAR_ARRAY_T, 0,
                        CHARFLDSET(struct global_options, exchange));

    if (aco_process_config(&cfg_info, 0))
    {
        goto load_error;
    }

    /* Unregister the hook, we don't want a double-registration */
    ast_manager_unregister_hook(&ami_hook);
    /* Register the hook for AMI events */
    ast_manager_register_hook(&ami_hook);

    return AST_MODULE_LOAD_SUCCESS;

load_error:
    aco_info_destroy(&cfg_info);
    return AST_MODULE_LOAD_DECLINE;
}

/*! \internal \brief reload handler
 * \retval AST_MODULE_LOAD_SUCCESS on success
 * \retval AST_MODULE_LOAD_DECLINE on failure
 */
static int reload_module(void)
{
    if (aco_process_config(&cfg_info, 1))
    {
        return AST_MODULE_LOAD_DECLINE;
    }

    /* Unregister the hook, we don't want a double-registration */
    ast_manager_unregister_hook(&ami_hook);
    /* Register the hook for AMI events */
    ast_manager_register_hook(&ami_hook);
    return 0;
}

/*! \internal \brief unload handler */
static int unload_module(void)
{
    /* Unregister the hook, we don't want a double-registration (Bad Things(tm) happen) */
    ast_manager_unregister_hook(&ami_hook);
    aco_info_destroy(&cfg_info);
    return 0;
}

AST_MODULE_INFO(ASTERISK_GPL_KEY, AST_MODFLAG_LOAD_ORDER, "Send all AMI events to AMQP", .load = load_module,
                .unload = unload_module, .reload = reload_module, .load_pri = AST_MODPRI_DEFAULT,
                .support_level = AST_MODULE_SUPPORT_EXTENDED);
